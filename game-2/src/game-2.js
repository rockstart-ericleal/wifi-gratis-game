var game;
var gameOptions = {
	birdGravity: 0,
	birdSpeed: 125,
	birdFlapPower: 300,
	minPipeHeight: 200,
	pipeDistance:[ 300, 380],
	pipeHole: [70, 100],
	localStorage: 'localStorage'
};

window.onload = function(){
	let gameConfig = {
		type: Phaser.AUTO,
		backgroundColor: 0x022651,
		scale: {
			mode: Phaser.Scale.FIT,
			autoCenter: Phaser.Scale.CENTER_BOTH,
			parent: 'container',
			width: window.innerWidth,
			height: window.innerHeight,	
		},
		physics: {
			default: "arcade",
			arcade:{
				gravity: {
				 	y: 0
				},
				debug: false,
			}
		},
		scene: [ PlayGame, ]
	};

	game = new Phaser.Game(gameConfig);
	window.focus();

}

class PlayGame extends Phaser.Scene{
	
	constructor(){
		super('PlayGame');
	}

	preload(){

		//Particles
		this.load.image('spark', '../assets/particles/muzzleflash3.png');

		//Assets
		this.load.path = './assets/'
		this.load.image('bird','bird.png');
		
		this.load.spritesheet('pipe-1','pipe-1.png', { frameWidth: 64, frameHeight: 1230 });
		this.load.image('pipe-2','pipe-2.png');
		this.load.image('pipe-3','pipe-3.png');
		this.load.image('pipe-4','pipe-4.png');
		this.load.image('wifito-1','wifito_1.png');

		this.load.spritesheet('wifito','spritesheet___.png', { frameWidth: 101, frameHeight: 101 });
		this.load.spritesheet('power-bg','power_bg.png', { frameWidth: 182, frameHeight: 206 });
		// this.load.animation('spritesheet', 'spritesheet.json');

		//Ads
		this.load.image('add-1', '/ads-1.jpg');
		this.load.image('cocacola', '/cocacola.png');


		//Game States
		this.START_GAME = false;
		this.END_GAME = true;
		this.PAUSE_GAME = true;


		//Sounds
		this.load.audio('fx-winner', ['sounds/winner_1.mp3']);
		
	}

	create(){

		//Camera
		this.camera = this.cameras.main;

		this.bgAd = this.add.image(0, 0,'add-1');
		this.bgAd.setDisplaySize(this.scale.width, this.scale.height);
		this.bgAd.setPosition(this.scale.width/2, this.scale.height/2);
		this.bgAd.alpha = 0.7;

		let particles = this.add.particles('spark');
        let emitter = particles.createEmitter({
	        alpha: { start: 1, end: 0 },
	        scale: { start: 0.1, end: 1.5 },
	        //tint: { start: 0xff945e, end: 0xff945e },
	        speed: 20,
	        accelerationX: -500,
	        angle: { min: -85, max: -95 },
	        rotate: { min: -180, max: 180 },
	        lifespan: { min: 1000, max: 1100 },
	        blendMode: 'ADD',
	        frequency: 30,
	        //maxParticles: 10,
	        x: -25
	    });


  //       particles.createEmitter({
	 //        speed: 50,
	 //        scale: { start: .2, end: 0},
	 //        blendMode: 'ADD',
	 //        gravityX: -1550,
	 //        gravityY: 100,
	 //        lifespan: 1500,
	 //        x:-30
		// });

        
        this.premioGroup = this.physics.add.group();

		this.anims.create({
			key: 'wififly',		    
		    frames: this.anims.generateFrameNumbers('power-bg', { start: 0, end: 15 }),
		    frameRate: 20,
		    repeat: -1
		});
		//this.powerBg = this.add.sprite(0, 0, 'power-bg');
		//this.powerBg.anims.play('wififly');

		this.cocacola = this.add.image(100, 100,'cocacola');
        this.cocacola.setScale(0.12);
        this.cocacola.setAngle(45);

		
		this.fireTweens = this.add.tween({
			targets:[this.cocacola],
			ease:'Sine.easeInOut',
			// y:(this.game.config.height) -30,
			angle: -45,
			repeat: -1,
			yoyo: true,
			duration:3000

		});

		//this.powerBg.setPosition(this.cocacola.x, this.cocacola.y);
		//this.powerBg.setAlpha(.7);

		//this.premioGroup.add(this.powerBg);
		this.premioGroup.add(this.cocacola);
		
		this.anims.create({
			key: 'trap',		    
		    frames: this.anims.generateFrameNumbers('pipe-1', { start: 0, end: 32 }),
		    frameRate: 45,
		    repeat: -1
		});

	

		this.pipeGroup = this.physics.add.group();
		this.pipePool = [];
		
		for ( let i = 0; i < 4; i++){			
			this.pipePool.push(this.pipeGroup.create(0, 0, 'pipe-1'));
			this.pipePool.push(this.pipeGroup.create(0, 0, 'pipe-1'));
			this.pipeGroup.getChildren()[i].anims.play('trap');

			this.placePipes(false);
		}
		this.pipeGroup.setVelocityX(0);
		this.bird = this.physics.add.sprite(80, game.config.height / 2, 'wifito-1');
		this.bird.setScale(1)
		this.bird.body.gravity.y = gameOptions.birdGravity;
		this.bird.body.setCircle(28);
		//this.bird.body.setSize(70, 50);
		this.bird.body.setOffset(28, 15);

		this.input.on('pointerdown', this.flap, this);
		this.score = 0;
		this.topScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : localStorage.getItem(gameOptions.localStorageName);
		this.scoreText = this.add.text(10, 10, '');
		this.updateScore(this.score);

		
		

		this.premioWiner();
		this.sumPoints = true;
		
	    

	    emitter.startFollow(this.bird);

	    let style = { font: "bold 64px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
	    this.winnerPointsText = this.add.text(-200, -200, '+5', style);
	    this.winnerPointsText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
	    
	    
	    this.winnerSound = this.sound.add('fx-winner');


	}

	premioWiner(){
		let rangePosition = this.game.scale.height/6;
		let positionRandom = Phaser.Math.Between(rangePosition, this.game.scale.height-rangePosition);
	    this.premioGroup.setXY(this.game.scale.width + positionRandom, positionRandom);
	    this.premioGroup.setVelocityX(-160);  
	    this.sumPoints = true; 
	    this.premioGroup.getChildren()[0].setScale(0.1);
	}

	update(){
		this.physics.world.collide(this.bird, this.pipeGroup, function(){
            this.die();
        }, null, this);
        if(this.bird.y > game.config.height || this.bird.y < 0){
            this.die();
        }

        this.pipeGroup.getChildren().forEach(function(pipe){
            if(pipe.getBounds().right < 0){
                this.pipePool.push(pipe);
                if(this.pipePool.length == 2){
                    this.placePipes(true);
                }
            }
        }, this);



        if(this.distance(this.bird.x, this.bird.y, this.premioGroup.getChildren()[0].x, this.premioGroup.getChildren()[0].y) <= 50){        	
        	if(this.sumPoints){
        		this.updateScore(5);
        		this.sumPoints = false;
        		
        		this.add.tween({
        			targets:[this.premioGroup.getChildren()[0]],
        			ease:'Sine.easeInOut',
        			repeat: 0,
        			x:this.bird.x,
        			y:this.bird.y,
					yoyo: false,
					duration:300,
					scale: 0,
        		});

        		this.winnerSound.play();

        		this.winnerPointsText.x = this.bird.x;
        		this.winnerPointsText.y = this.bird.y;
        		this.winnerPointsText.setAlpha(1);

        		this.add.tween({
        			targets:[this.winnerPointsText],
        			ease:'Expo.easeInOut',
        			alpha: 0,
        			repeat: 0,
        			x:200,
        			y:100,
					yoyo: false,
					duration:3000,					
        		});
        	}
        	
        }
	}

	distance(x1, y1, x2, y2) {
        var dx = x1 - x2;
        var dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

	flap(){

        this.bird.body.velocity.y = -gameOptions.birdFlapPower;
        this.bird.body.gravity.y = 800;
        this.pipeGroup.setVelocityX(-gameOptions.birdSpeed);
    }

	placePipes(addScore){

		//let texturePipe = 'pipe-' + Phaser.Math.Between(1,4);	
		let texturePipe = 'pipe-1';
		let rightmost = this.getRightmostPipe();
		let pipeHoleHeight = Phaser.Math.Between(gameOptions.pipeHole[0], gameOptions.pipeHole[1]);
		let pipeHolePosition = Phaser.Math.Between(gameOptions.minPipeHeight + pipeHoleHeight / 2, game.config.height - gameOptions.minPipeHeight - pipeHoleHeight / 2);
		

		this.pipePool[0].x = rightmost + this.pipePool[0].getBounds().width + Phaser.Math.Between(gameOptions.pipeDistance[0], gameOptions.pipeDistance[1]);
		this.pipePool[0].y = pipeHolePosition - pipeHoleHeight / 2;
		this.pipePool[0].setOrigin(0, 1);
		this.pipePool[0].setTexture(texturePipe);
		this.pipePool[0].anims.play('trap');
		this.pipePool[0].setSize(45, 1220);
		this.pipePool[0].setOffset(10, 0);
		

		
		this.pipePool[1].x = this.pipePool[0].x;
		this.pipePool[1].y = pipeHolePosition + pipeHoleHeight / 1;
		this.pipePool[1].setOrigin(0, 0);
		this.pipePool[1].setTexture(texturePipe);
		this.pipePool[1].anims.play('trap');
		this.pipePool[1].setSize(45, 1220);
		this.pipePool[1].setOffset(10, 6);

		

		this.pipePool = [];
		if(addScore){
            this.updateScore(1);
        }

	}
	getRightmostPipe(){
        let rightmostPipe = 0;
        this.pipeGroup.getChildren().forEach(function(pipe){
            rightmostPipe = Math.max(rightmostPipe, pipe.x);

        });
        return rightmostPipe;
    }
	updateScore(inc){
		console.log(this.sumPoints);		
		if(this.premioGroup.getChildren()[0].x < -100){
			this.premioWiner();
		}

        this.score += inc;
        this.scoreText.text = 'Score: ' + this.score + '\nBest: ' + this.topScore;
    }

    die(){
        localStorage.setItem(gameOptions.localStorageName, Math.max(this.score, this.topScore));
        //this.scene.pause('PlayGame');
        this.scene.start('PlayGame');
    }
}