import { log } from '../dist/utils-min.js';
import gameOptions from '../dist/game-config-min.js';

class PlayGame extends Phaser.Scene{
	constructor(){
		super("PlayGame");
	}

	preload(){

		//Images
		this.load.path = './assets/'
		this.load.image("ground", "ground.png");
		this.load.image("bgg", "bgg.png");
		this.load.image("bg-ad", "ads/ads-1.jpg");
		
		this.load.image('blue', 'particles/blue_.png');
		this.load.image('explo-fire', 'particles/fire.png');
		this.load.image("ball", "ball.png");
		this.load.image("star", "stars.png");
		this.load.image("fire", "fire.png");
		this.load.image("icon_king", "icon_king.png");
		this.load.image("icon_pausa", "icon_pausa.png");

		//Sounds
		this.load.audio('fx-bounce', ['sound/bounce-ball-1.mp3']);
		this.load.audio('fx-sound-bg', ['sound/sound-bg.mp3']);
		this.load.audio('fx-fire', ['sound/fire.mp3']);

		//Fonts
		this.load.image('number-font', 'fonts/number-font.png');
		this.load.json('number-font-json','fonts/number-font.json');

		//Ads
		this.load.image('add-1', 'ads/ads-1.jpg');
		
	}

	create(){


		this.templateScore = '000000';

		//
		this.soundFireActive = false;
		let rnd = new Phaser.Math.RandomDataGenerator((Date.now() * Math.random()).toString());
		this.sound.stopAll();
		//Cameras
		this.camera = this.cameras.main;

		//Sounds
		this.bounceSound = this.sound.add('fx-bounce');
		this.bounceSound.volume = 0.7;
		
		// this.bgSound = this.sound.add('fx-sound-bg');
		// this.bgSound.loop = true;
		// this.bgSound.play();
		// this.bgSound.volume = 0.2;

		this.fireSound = this.sound.add('fx-fire');		

		//Create Group which will contain all platforms
		this.platformGroup = this.physics.add.group();
		this.fireGroup = this.add.group();
		
		this.bgg = this.add.image(0, 0, 'bgg');
		this.bgAd = this.add.image(0, 0,'bg-ad');
		this.bgAd.setDisplaySize(this.scale.width, this.scale.height);
		this.bgAd.setPosition(this.scale.width/2, this.scale.height/2);
		this.bgAd.alpha = 0.8;

		this.starsArray = [];
		for(let s = 0; s < gameOptions.amount_stars; s++){
			let xStar = Phaser.Math.Between(this.game.config.width + 100, window.innerWidth);
			let yStar = Phaser.Math.Between(100, this.game.config.height*2 -30);

			this.starBg = this.add.sprite(xStar, yStar, 'star');
			this.starBg.velocity = 0.2 + rnd.frac() * 2;
			this.starBg.alpha = rnd.frac() - 0.3;
			this.starBg.scale = (0.3 + rnd.frac());
			this.starsArray[s] = this.starBg;
		}

		// let delay = 0;
		// let distanceX = 10;
		// for ( let i = 0; i < ( Math.max((this.game.config.width*2)/10) ); i++ ){
  //       	let fire = this.fireGroup.create(distanceX, this.game.config.height, "fire" ); 
  //       	fire.scale = (0.45 + rnd.frac()/2);

  //       	let speed = Phaser.Math.Between(500, 2000);
  //       	this.fireTweens = this.add.tween({
	 //        	targets:[fire],
	 //        	ease:'Sine.easeInOut',
	 //        	y:(this.game.config.height) -30,
	 //        	repeat: -1,
	 //        	yoyo: true,
	 //        	duration:speed,
	 //        	delay:delay

	 //        });

	 //        delay += 0;
	 //        distanceX += 20;
  //       }


  //       let ad = this.add.image((this.scale.width * 2), this.scale.height/3, 'add-1');
		// ad.setScale(0.5);
		// ad.setAlpha(.8);
		
		// this.adAnimate = this.add.tween({
  //       	targets:[ad],
  //       	ease:'Sine.easeInOut',
  //       	x:-(this.scale.width ),
  //       	repeat: 10,
  //       	yoyo: false,
  //       	duration:10000,
  //       	delay:0

  //       });
        

		this.bgg.setDisplaySize(this.game.config.width*2,this.game.config.height*2);

		this.ball = this.physics.add.image(this.game.config.width * gameOptions.ballPosition, this.game.config.height * gameOptions.groundPosition - gameOptions.ballHeight, "ball");
		this.ball.body.gravity.y = gameOptions.ballGravity;
		this.ball.setBounce(1);
		this.ball.setScale(0.7);
		this.ball.body.checkCollision.down = true;
        this.ball.body.checkCollision.up = false;
        this.ball.body.checkCollision.left = false;
        this.ball.body.checkCollision.right = false;
        this.ball.setSize(30, 50, true);
        this.ball.body.setCircle(18);
        this.ball.body.setOffset(18, 15);

        //First Platform 
        let platformX = this.ball.x;

        for ( let i = 0; i < 5; i++ ){
        	let platform = this.platformGroup.create(0, 0, "ground" );
        	platform.setImmovable(true);
        	platform.body.setSize(platform.width-20, 50);
        	platform.body.setOffset(10, 10);
        	platform.setScale(0.5);

        	this.placePlatform(i, platform, platformX);
        	platformX += Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1]);
        }

        this.input.on("pointerdown", this.movePlatforms, this);
        this.input.on("pointerup", this.stopPlatforms, this);

        this.score = 0;

        //NO FUNCIONA EN PORTAL CAUTIVO
        this.topScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : localStorage.getItem(gameOptions.localStorageName);

        this.scoreText = this.add.text(10, 10, "");     
        
        let particles = this.add.particles('blue');
        let emitter = particles.createEmitter({
	        speed: 50,
	        scale: { start: .4, end: 0},
	        blendMode: 'ADD',
	        gravityY: 350,
	        lifespan: 500
		});
	    emitter.startFollow(this.ball);


	    let exploFire = this.add.particles('explo-fire');
	    this.explotion = exploFire.createEmitter({
	    	active:false, 
	    	yoyo: false, 
	    	quantity: 1,
	    	scale: { start: .8, end: 0},
	    });
	    this.explotion.setLifespan(600);
	    this.explotion.setSpeed(200);



	    //Fonts
		const numberFontConfig = this.cache.json.get('number-font-json');
		log(numberFontConfig)
		this.cache.bitmapFont.add('number-font-render', Phaser.GameObjects.RetroFont.Parse(this, numberFontConfig));
		this.numberScoreText = this.add.bitmapText(0, 0, 'number-font-render', this.templateScore, 54, 1);
		this.numberScoreText.letterSpacing = -48;	

		this.numberScoreText.setPosition( ((this.game.config.width/2)-(this.numberScoreText.width/2))+18, 54 )
		log(this.numberScoreText);

		this.updateScore(0);

		this.titleScore = this.add.bitmapText(0, 0, 'number-font-render', this.templateScore, 24, 1);
		this.titleScore.setPosition( ((this.game.config.width/2)-(this.titleScore.width/2))+21, 24 );
		this.titleScore.text = 'SCORE';


		this.bestScore = this.add.bitmapText(0, 0, 'number-font-render', this.templateScore, 21, 1);
		this.bestScore.text = 'BEST SCORE: '+ this.topScore;
		this.bestScore.setOrigin(0.5);
		this.bestScore.setPosition( (this.scale.width/2)+8 , this.titleScore.height + this.numberScoreText.height + 42 );		
		this.bestScore.alpha = .5;
		this.bestScore.letterSpacing = -50

		// this.iconKing = this.add.image(0, 0, 'icon_king');
		// this.iconKing.setScale(0.5);
		// this.iconKing.setPosition(28, 28);
		// this.iconKing.setInteractive();
		// this.iconKing.on(Phaser.Input.Events.POINTER_DOWN, () => {
		// 	log('Click in BTN KING');
		// });

		// this.iconPausa = this.add.image(0, 0, 'icon_pausa');
		// this.iconPausa.setScale(0.5);
		// this.iconPausa.setPosition(this.scale.width-28, 28);
		// this.iconPausa.setInteractive();
		// this.iconPausa.on(Phaser.Input.Events.POINTER_DOWN, () => {
		// 	log('Click in BTN PAUSA');
		// });


		

	}

	update(){
		let c = this.camera;
		let bs = this.bounceSound;

		this.physics.world.collide(this.platformGroup, this.ball, function(platform, ball){
        	if(ball.fallingDown){
        		ball.body.gravity.y = 1000;
        	}
        	c.shake(50, 0.01)
        	bs.play();	
        	// log(platform.body.bounce.y, this);
        });

        this.platformGroup.getChildren().forEach(function(platform){
        	if (platform.getBounds().right < 0 ){
        		this.updateScore(1);
        		this.placePlatform(1,platform, this.getRightmostPlatform() + Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1]) );
        	}
        }, this);
     
        if(this.ball.y > this.game.config.height){         
           	
           	

            log(this.fireSound);
          	if (this.soundFireActive == false){
          		this.fireSound.play();
          		//NO FUNCIONA EN PORTAL CAUTIVO
	            localStorage.setItem(gameOptions.localStorageName, Math.max(this.score, this.topScore));         
	            this.explotion.setBlendMode(Phaser.BlendModes.ADD);
	            this.explotion.setPosition(this.ball.x, this.game.config.height);
	            this.explotion.active = true;

	            let e = this.explotion;
	   			let es = this.scene   			
	            this.time.delayedCall(2500, function() {			    
				    e.stop()
				    es.start("PlayGame");
				});
          	}
          	this.soundFireActive = true;

            // this.explotion.tint(0xff0000);   			
         
        }


        for(let s = 0; s < gameOptions.amount_stars; s++){						
			let starBg = this.starsArray[s];
			starBg.x -= starBg.velocity;
			if(starBg.x <= -100){
				starBg.x = Phaser.Math.Between(window.innerWidth + 100, window.innerWidth);
				starBg.y = Phaser.Math.Between(100, window.innerHeight*2 -30);
			}
			//log(starBg.x);
		}

	}

	getRightmostPlatform(){
		let rightmostPlatform = 0;
		this.platformGroup.getChildren().forEach(function(platform){
			rightmostPlatform = Math.max(rightmostPlatform, platform.x);
		}, this);
		return rightmostPlatform;
	}

	updateScore(currentScore){
		this.score += currentScore;

		let n = this.score.toString();
		log(n.length);
		let newTemplate = this.templateScore;
		let formatTemplate = newTemplate.slice(0, this.templateScore.length -  n.length);
		this.numberScoreText.text = formatTemplate + this.score.toString();


		// this.scoreText.text = "Score: " + this.score + "\n Best: " + this.topScore;
	}

	movePlatforms(){
		this.platformGroup.setVelocityX(-gameOptions.platformSpeed);
	}

	stopPlatforms(){
		this.platformGroup.setVelocityX(0);
	}

	placePlatform(index, platform, posX){
		platform.x = posX;
		//Variante de altura de plataformas.
		platform.y = this.game.config.height * gameOptions.groundPosition + Phaser.Math.Between(gameOptions.platformHeightRange[0], gameOptions.platformHeightRange[1]);

		//Permitir caer a la plataforma
		log(posX != this.ball.x && Phaser.Math.Between(1, 100) <= gameOptions.fallingPlatformPercent)
		//platform.fallingDown = posX != this.ball.x && Phaser.Math.Between(1, 100) <= gameOptions.fallingPlatformPercent;
		log('INDEX', index);
		if(index == 0){
			platform.fallingDown = false;
		}else{
			platform.fallingDown = false;
		}

		platform.displayWidth = Phaser.Math.Between(gameOptions.platformLengthRange[0], gameOptions.platformLengthRange[1]);

		platform.body.gravity.y = 0;
		platform.body.velocity.y = 0;
	}

}


export default PlayGame;