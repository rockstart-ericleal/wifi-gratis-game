const gameOptions = {
	groundPosition: (4 / 6),
	ballHeight: 360,
	ballGravity: 1500,
	ballPosition: (1/4),
	platformSpeed: 650,
	platformDistanceRange: [160, 300],
	platformHeightRange: [0, 40],
	platformLengthRange: [50, 100],
	platformLengthPercent: 50,
	fallingPlatformPercent: 2,
	localStorageName: "score-wg",
	amount_stars: 16
};

export default gameOptions;
