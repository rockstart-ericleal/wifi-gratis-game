//import Phaser from 'phaser'
//import gameOptions from 'game-config'
import PlayGame from '../dist/play-game-min.js'

window.onload = function(){

	try {
		
		let config = {
			type: Phaser.AUTO,
			backgroundColor: 0x022651,
			scale: {
				mode: Phaser.Scale.FIT,
				autoCenter: Phaser.Scale.CENTER_BOTH,
				parent: "container",
				width: window.innerWidth,
				height: window.innerHeight,	
			},
			physics: {
				default: "arcade",
				arcade:{
					// gravity: {
					// 	y:800
					// },
					// debug: true,
				}
			},
			scene: [ PlayGame, ]
		}

		const game = new Phaser.Game(config);
		window.focus();

	}
	catch(error) {
	  
	  let el = document.querySelector("body");
	  el.innerHTML = 'err + ' + error;
	}

}


window.onerror = function(errorMsg, url, lineNumber) {
	console.log(errorMsg,url,lineNumber);
    let el = document.querySelector("body");
	el.innerHTML = '<h5>'+errorMsg+'</h5><a href="'+url+'">'+url+'</a><h5>'+lineNumber+'</h5>';
};





